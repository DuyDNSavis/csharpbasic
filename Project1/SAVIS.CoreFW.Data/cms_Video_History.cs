//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NewCMS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class cms_Video_History
    {
        public long VideoHistoryId { get; set; }
        public System.Guid VideoId { get; set; }
        public string Action { get; set; }
        public System.DateTime CreatedOnDate { get; set; }
        public System.Guid CreatedByUserId { get; set; }
    
        public virtual cms_Video cms_Video { get; set; }
    }
}
