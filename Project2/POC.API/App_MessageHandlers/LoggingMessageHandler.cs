﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Threading.Tasks;


namespace POC.API.App_MessageHandlers
{
    /// <summary>
    /// This handler will return to inner handler
    /// </summary>
    public class LoggingMessageHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {

            // Logging
            // LogService.IpLogger.Info(GetClientIpAddress(request) + " | " +  request.RequestUri);

            // Inner process
            return base.SendAsync(request, cancellationToken);
        }
        
        public static string GetClientIpAddress(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;

            //if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            //    return ((RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name]).Address;

            return "IP Address Unavailable";    //here the user can return whatever they like
        }

    }
}