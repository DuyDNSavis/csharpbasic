﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Threading.Tasks;
using System.Configuration;
using System.Web.Http;


namespace POC.API.App_MessageHandlers
{
    /// <summary>
    /// This handler validates date time input from the query string 
    /// to search API
    /// </summary>
    public class KeyValidationMessageHandler : DelegatingHandler
    {
        // private IApiKeyValidator keyValidator;
        private const string BasicAuthResponseHeader = "WWW-Authenticate";
        private const string BasicAuthResponseHeaderValue = "Basic";

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {

            // Continue if it's an option request
            if (request.Method.Method == "OPTIONS") return base.SendAsync(request, cancellationToken);

            if (!ValidateKey(request))
            {
                // Create new response with 401 : Unauthorized
                var result = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    RequestMessage = request,
                    Content = new ObjectContent<string>("401 - Unauthorized", GlobalConfiguration.Configuration.Formatters.JsonFormatter, "application/json")
                };

                //we need to include WWW-Authenticate header in our response,
                //so our client knows we are using HTTP authentication
                result.Headers.Add(BasicAuthResponseHeader, BasicAuthResponseHeaderValue);

                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(result);

                //// Logging
                //// LogService.IpLogger.Error(GetClientIpAddress(request) + " | Attached Wrong API Key(" + HttpStatusCode.Unauthorized + ") | " + request.RequestUri);

                return tsc.Task;
            }
            
            // return to inner handlers
            return base.SendAsync(request, cancellationToken);
        }

        public static string GetClientIpAddress(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;

            return "IP Address Unavailable";    //here the user can return whatever they like
        }

        /// <summary>
        /// Vaidate input date string
        /// Date is in format of : hh-mm-ss-dd-MM-yyyy
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool ValidateKey(HttpRequestMessage request)
        {
            // Get authorization value from header
            AuthenticationHeaderValue authValue = request.Headers.Authorization;
            
            if (authValue == null || String.IsNullOrWhiteSpace(authValue.Parameter)
                || String.IsNullOrWhiteSpace(authValue.Scheme)
                || authValue.Scheme != "Basic")
            {
                return false;
            }


            string authorizationKey = authValue.Parameter;

            return (authValue.Scheme == "Basic" && authorizationKey == ConfigurationManager.AppSettings["api:Token"]);

        }
    }
}