﻿using POC.Business.Config;
using POC.Common;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Principal;
using System.Configuration;

namespace POC.API
{
    public static class ApiUtils
    {
        static ILogService logger = BusinessServiceLocator.Instance.GetService<ILogService>();

        public static CustomRequestHeaderValue GetRequestFields(HttpRequestMessage request, IPrincipal principal)
        {
            var authConfig = ConfigurationManager.AppSettings["Authority:enable"];

            var result = new CustomRequestHeaderValue {
                UserId = Guid.Empty,
                ModuleId = Guid.Empty
            };

            if (!principal.Identity.IsAuthenticated && authConfig.Equals("1"))
            {
                logger.Error("Unauthorize request | url : " + request.RequestUri + " | auth config : " + authConfig);
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
                

            string userId = request.Headers.Contains("X-User") ?
                request.Headers.GetValues("X-User").FirstOrDefault() : string.Empty;

            logger.Debug("User found in X-User : " + userId);

            if (string.IsNullOrEmpty(userId) &&
                principal != null && principal.Identity.IsAuthenticated)
            {
                userId = principal.Identity.Name;

                logger.Debug("User found in Identity : " + userId);

            }
            string moduleId = request.Headers.Contains("X-Module") ? 
                request.Headers.GetValues("X-Module").FirstOrDefault() : string.Empty;

            string applicationId = request.Headers.Contains("X-ApplicationId") ?
                request.Headers.GetValues("X-ApplicationId").FirstOrDefault() : string.Empty;

            if (!string.IsNullOrEmpty(userId) && Utils.IsGuid(userId))
            {
                result.UserId = new Guid(userId);
            }

            if(!string.IsNullOrEmpty(moduleId))
            {
                result.ModuleId = new Guid(moduleId);
            }

            if (!string.IsNullOrEmpty(applicationId) && Utils.IsGuid(applicationId))
            {
                result.ApplicationId = new Guid(applicationId);
            }

            return result;
        }

        public static void WriteApiLog(string message, CustomRequestHeaderValue requestData)
        {
            var log = BusinessServiceLocator.Instance.GetService<ILogService>();

            log.Info(message, requestData.UserId, requestData.ApplicationId);
        }

        public static void WriteApiLog(string message, HttpRequestMessage request, IPrincipal principal)
        {
            var requestData = GetRequestFields(request, principal);

            var log = BusinessServiceLocator.Instance.GetService<ILogService>();

            log.Info(message, requestData.UserId, requestData.ApplicationId);
        }
    }
    public class CustomRequestHeaderValue
    {
        public Guid UserId { get; set; }
        public Guid ModuleId { get; set; }
        public Guid ApplicationId { get; set; }

    }
}