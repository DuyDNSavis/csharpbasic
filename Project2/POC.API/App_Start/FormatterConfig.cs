﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace POC.API
{
    public class FormatterConfig
    {
        public static void RegisterFormatters()
        {
            #region Data Formatter Registration
            /*
             JSON Media-Type Formatter
             * JSON formatting is provided by the JsonMediaTypeFormatter class. 
             * By default, JsonMediaTypeFormatter uses the Json.NET library to perform serialization. 
             * Json.NET is a third-party open source project.
             * If you prefer, you can configure the JsonMediaTypeFormatter class to use the DataContractJsonSerializer instead of Json.NET. 
             * To do so, set the UseDataContractJsonSerializer property to true
             * 
             * NOTE : If you UseDataContractJsonSerializer you must add Attributes [DataContract] and [DataMember] on class & field
             * if not, an exception of Serialization will be throw
             * 
             */
            // Remove the usage of DataContractJsonSerializer / Use JSON.NET only
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.UseDataContractJsonSerializer = false;
            
            // Format as camelCase to client
            //HttpConfiguration config = GlobalConfiguration.Configuration;
            //config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            /*
             * XML formatting is provided by the XmlMediaTypeFormatter class. 
             * By default, XmlMediaTypeFormatter uses the DataContractSerializer class to perform serialization.
             * XmlMediaTypeFormatter is default .NET library
             * 
             * If you prefer, you can configure the XmlMediaTypeFormatter to use the XmlSerializer instead of the DataContractSerializer. 
             * To do so, set the UseXmlSerializer property to true:
             */

            // Use XMLSerializer instead of DataContractSerializer
            var xml = GlobalConfiguration.Configuration.Formatters.XmlFormatter;
            xml.UseXmlSerializer = true;
            #endregion
        }
    }
}