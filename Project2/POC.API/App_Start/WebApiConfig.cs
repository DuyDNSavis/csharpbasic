﻿using POC.API.App_MessageHandlers;
using Newtonsoft.Json.Converters;
using System.Configuration;
using System.Web.Http;

namespace POC.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.EnableCors();
        }

        public static HttpConfiguration Register()
        {
            var config = new HttpConfiguration();

            // Add configuration for authorize using bearer token - update 11/05/2016
            var configStr = ConfigurationManager.AppSettings["Authority:enable"];
            if (configStr.Equals("1")) config.Filters.Add(new AuthorizeAttribute());

            // Web API configuration and services
            config.EnableCors();

            // Web API routes
            config.MapHttpAttributeRoutes();

            // Add message handlers
            AddMessageHandlers(config);

            // Add custom authorization filters
            AddCustomAuthorizationFilters(config);

            // Register routes
            RegisterUploadRoute(config);

            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.IsoDateTimeConverter());
            //config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new MyDateTimeConverter());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            return config;
        }

        /* This function add message handlers for application */
        public static void AddMessageHandlers(HttpConfiguration config)
        {
            // config.MessageHandlers.Add(new KeyValidationMessageHandler());
            config.MessageHandlers.Add(new LoggingMessageHandler());
        }

        /* This function will add filter to all requests, any request must satisfy filter condition to pass to controller */
        public static void AddCustomAuthorizationFilters(HttpConfiguration config)
        {
            // config.Filters.Add(new BasicAuthorizeAttribute());
        }

        public static void RegisterUploadRoute(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "UploadPresets",
                routeTemplate: "api/upload/presets",
                defaults: new { controller = "Upload"}
            );
        }
    }
}
