﻿using System.Web;
using System.Web.Mvc;

namespace POC.API
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            // Add global attribute
            // GlobalFilters.Filters.Add(new System.Web.Http.AuthorizeAttribute());
        }
    }
}
