﻿using POC.Business;
using POC.Business.Config;
using POC.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Linq;

namespace POC.API.Controllers
{
    /// <summary>
    /// API quản lý các archivetype 
    /// </summary>
    public class VideoController : ApiController
    {
        readonly IVideoHandler _videoHandler = BusinessServiceLocator.Instance.GetService<IVideoHandler>();

        #region GET
        [HttpGet]
        [Route("api/v3/cms/videos/{videoId}")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoModel> GetVideoById(Guid videoId)
        {
            return _videoHandler.GetVideoById(videoId);

        }
        [HttpGet]
        [Route("api/v3/cms/videos")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<IList<VideoModel>> GetFilter(string filter)
        {
            VideoQueryFilter filterConvert = JsonConvert.DeserializeObject<VideoQueryFilter>(filter);
            var videos = _videoHandler.GetFilter(filterConvert);
            return videos;
        }
        [HttpGet]
        [Route("api/v3/cms/videos/validate")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoModel> GetByTitle(string title)
        {
            var videos = _videoHandler.GetByTitle(title);
            return videos;
        }
        [HttpGet]
        [Route("api/v3/cms/videos/{videoId}/profiles")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoProfile> GetVideoProfileByVideoId(Guid videoId)
        {
            return _videoHandler.GetVideoProfileByVideoId(videoId);
        }
        [HttpGet]
        [Route("api/v3/cms/videos/{videoId}/profiles/{videoProfileId}")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoProfile> GetVideoProfileById(Guid videoId, Guid videoProfileId)
        {
            return _videoHandler.GetVideoProfileById(videoId, videoProfileId);
        }
        [HttpGet]
        [Route("api/v3/cms/videos/{videoId}/profiles")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<IList<VideoProfile>> GetVideoProfileFilter(Guid videoId)
        {
            var videos = _videoHandler.GetVideoProfilesByVideoId(videoId);
            return videos;
        }
        #endregion

        #region ADD,UPDATE,DELETE
        [HttpPost]
        [Route("api/v3/cms/videos")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoModel> AddVideo([FromBody] VideoCreateRequestModel model)
        {
            return _videoHandler.CreateVideo(model);
        }

        [HttpPut]
        [Route("api/v3/cms/videos/{videoId}")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoModel> UpdateVideo(Guid videoId, [FromBody] VideoUpdateRequestModel model)
        {
            return _videoHandler.UpdateVideo(videoId, model);
        }

        [HttpDelete]
        [Route("api/v3/cms/videos/{videoId}")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoDeleteResponseModel> DeleteVideo(Guid videoId)
        {
            var result = _videoHandler.DeleteVideo(videoId);
            return result;
        }
        [HttpPut]
        [Route("api/v3/cms/videos/{videoId}/totalview")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoModel> UpdateVideoTotalView(Guid videoId)
        {
            return _videoHandler.UpdateVideoTotalView(videoId);
        }
        [HttpPost]
        [Route("api/v3/cms/videos/{videoId}/profiles")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoProfile> CreateVideoProfileByVideoId(Guid videoId, [FromBody] VideoProfileCreateRequestModel videoProfile)
        {
            return _videoHandler.CreateVideoProfileByVideoId(videoId, videoProfile);
        }
        [HttpDelete]
        [Route("api/v3/cms/videos/{videoId}/profiles/{videoProfileId}")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public Response<VideoProfile> DeleteVideoProfile(Guid videoId, Guid videoProfileId)
        {
            return _videoHandler.DeleteVideoProfile(videoId, videoProfileId);
        }
        #endregion

        #region Generate sample videos
        [HttpGet]
        [Route("api/v3/cms/videos/generate")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Authorize]
        public string AddVideo()
        {
            // 
            List<string> listVideoName = new string[] {
                "Shape of you",
                "The amazing Spider-Man",
                "Billy the kid",
                "Best voice ever",
                "Mee at the zoo",
                "Gone with the wind",
                "John Wick",
                "The departed" }.ToList();
            List<string> listUserId = new string[] { "2CCC1816-C4EA-4F6D-9D3D-D7027D0C3508",
                "D7A8C632-E37A-4411-8C2E-69B46D079037", "C670B266-4F78-4718-9E09-63CB7BDCA503" }.ToList();


            //add 10k data
            for (int i = 0; i < 10; i++)
            {
                string strNameTest = GetRandomString(listVideoName) + " " + i;
                var model = new VideoCreateRequestModel();
                model.CreatedByUserId = new Guid(GetRandomString(listUserId));
                model.ApplicationId = new Guid("A9DA69BE-7F29-4FE0-AD2E-9B055FD454D2");
                model.Description = "This is the video about " + strNameTest;
                model.Duration = GetRandomInt(1200, 3000);
                model.ThumbnailPath = "http://123.31.18.155:88/res/te002/fe-be.jpg";
                model.StreamingProtocol = "http";
                model.ThumbnailDescription = strNameTest;
                model.Title = strNameTest;
                model.WorkflowCode = string.Empty;
                //video profile
                model.VideoProfile = new VideoProfileCreateRequestModel();
                model.VideoProfile.CreatedByUserId = new Guid(GetRandomString(listUserId));
                model.VideoProfile.AspectRatio = string.Empty;
                model.VideoProfile.Duration = model.Duration;
                model.VideoProfile.Name = "Profile 480p of " + strNameTest;
                model.VideoProfile.Order = 0;
                model.VideoProfile.PhysicalFilePath = string.Empty;
                model.VideoProfile.Resolution = "480p";
                model.VideoProfile.VideoCodec = "H264";

                _videoHandler.CreateVideo(model);
            }

            return "Added 10 videos";

        }
        private string GetRandomString(List<string> source)
        {
            var random = new Random(DateTime.Now.Millisecond);

            int index = random.Next(source.Count);

            var name = source[index];

            return name;
        }
        private int GetRandomInt(int min, int max)
        {
            var random = new Random(DateTime.Now.Millisecond);

            int index = random.Next(min, max);

            return index;
        }

        #endregion
    }
}
