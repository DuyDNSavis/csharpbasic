﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Owin;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using Thinktecture.IdentityServer.AccessTokenValidation;
using Thinktecture.IdentityModel.Owin;
using Thinktecture.IdentityModel.Client;
using System;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Text;
using Newtonsoft.Json;
using System.Web.Configuration;

[assembly: OwinStartup(typeof(POC.API.StartupAuth))]

namespace POC.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
    public partial class StartupAuth
    {
        /* Cách thức authorization sử dụng với OAuth2 
         * 
         * 1. Đầu tiên user vào Browser, khi này browser chưa có access_token, hệ thống redirect sang trang đăng nhập
         * 
         * 2. User đăng nhập, hệ thống trả về link có chứa access_token & redirect đến trang authComplete.html, js trên 
         * trang này sẽ save access_token vào Local Storage
         * 
         * 3. User sau khi đăng nhập thì thực hiện các request lên API (lấy thông tin các grid) , các request này khi thực hiện đều được 
         * authorize dạng Bearer (thực hiện trong hàm _request của Http Interceptor)
         * 
         * 4. Trên API đc cấu hình để validate Bearer token truyền từ client để xem token này còn giá trị hay không, nếu còn giá trị thì mới
         * process tiếp vào controller, nếu token không còn giá trị, API trả về code 401
         * 
         * 5. Client khi nhận được code 401 sẽ redirect lại sang trang đăng nhập để lấy token mới
         */

        protected TokenResponse RequestToken(string username, string password)
        {
            var baseAddress = WebConfigurationManager.AppSettings["Authority:uri"];
            baseAddress += "connect/token";

            var client = new OAuth2Client(
                new Uri(baseAddress),
                username,
                password);

            return client.RequestClientCredentialsAsync("api_access").Result;
        }

        IEnumerable<Claim> GetItemsAsync(List<Claim> claims)
        {
            foreach (var item in claims)
            {
                yield return item;
            }
        }
        private async Task<IEnumerable<Claim>> BasicAuthValidationFunction(string username, string password)
        {
            TokenResponse response = RequestToken(username, password);
            if (response.IsError)
            {
                return null;
            }
            if (response.AccessToken.Contains("."))
            {

                var parts = response.AccessToken.Split('.');
                var claimsStr = parts[1];

                var jsonString = Encoding.UTF8.GetString(Base64Url.Decode(claimsStr));
                var dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonString);

                var claims = new List<Claim>();

                foreach (var item in dictionary)
                {
                    var values = item.Value as IEnumerable<object>;

                    if (values == null)
                    {
                        claims.Add(new Claim(item.Key, item.Value.ToString()));
                    }
                    else
                    {
                        foreach (var value in values)
                        {
                            claims.Add(new Claim(item.Key, value.ToString()));
                        }
                    }
                }
                return await Task.FromResult<IEnumerable<Claim>>(claims);
                //return claims;
                //return await GetItemsAsync(claims);


            }
            return null;
        }

        public void Configuration(IAppBuilder app)
        {
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            /*
             * Cấu hình validate bearer token 
             * Đây là cấu hình validate bearer token gửi lên từ user, việc này sử dụng thư viện ngoài
             * nên không cần viết thêm các class, chỉ cần cấu hình endpoint là chạy
             */
 
            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = WebConfigurationManager.AppSettings["Authority:uri"],
                RequiredScopes = new[] { "api_access" },
                NameClaimType = "user_id"
            });



            /* 
             * Cấu hình validate Basic authen 
             * Basic authen ở đây được sử dụng để kết nối Client (js) với Server
             */ 
            app.UseBasicAuthentication("ECM.API", new BasicAuthenticationMiddleware.CredentialValidationFunction(BasicAuthValidationFunction));



            /* Cấu hình Resource authorization
             * Đây là cấu hình để truy cập phần phân quyền API, kết nối qua basic authen
             */
            app.UseResourceAuthorization(new AuthorizationManager(new AuthorizationManagerOptions
            {
                ValidateUserRoleEndpoint = WebConfigurationManager.AppSettings["Identity:uri"],
                ClientID = WebConfigurationManager.AppSettings["Identity:clientid"],
                SecretKey = WebConfigurationManager.AppSettings["Identity:secretkey"],

            }));
            app.UseWebApi(WebApiConfig.Register());
        }
    }
}
