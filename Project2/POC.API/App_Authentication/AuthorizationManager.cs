﻿using System.Linq;
using System.Threading.Tasks;
using Thinktecture.IdentityModel.Owin.ResourceAuthorization;
using System.Net.Http;
using System;


namespace POC.API
{
    public class AuthorizationManagerOptions
    {
        public string ValidateUserRoleEndpoint { get; set; }
        public string ClientID { get; set; }
        public string SecretKey { get; set; }



    }

    public class AuthorizationManager : ResourceAuthorizationManager
    {
        private AuthorizationManagerOptions authorizationManagerOptions;

        public AuthorizationManager(AuthorizationManagerOptions authorizationManagerOptions)
        {
            // TODO: Complete member initialization
            this.authorizationManagerOptions = authorizationManagerOptions;
        }
        public override Task<bool> CheckAccessAsync(ResourceAuthorizationContext context)
        {
            //context.Resource.First().Value => rights
            // context.Action.First().Value => access list
            //context.Principal.Identity.Name => user name
            string resource_name = context.Resource.First().Value;
            string action_name = context.Action.First().Value;
            //var checkACL = true; 
            if (context.Principal.Identity.IsAuthenticated)
            {
                if (context.Principal.Identity.AuthenticationType == "Bearer")
                {
                    //Use access token to access resource
                    //string user_name = context.Principal.Identity.Name;
                    string user_name = context.Principal.Claims.FirstOrDefault(x => x.Type == "sub").Value;

                    //IAccessRightRoleHanler AccessRightRoleHanler = BusinessServiceLocator.Instance.GetService<IAccessRightRoleHanler>();
                    //checkACL = AccessRightRoleHanler.checkAccessRightUser(user_name, action_name, resorce_name);
                    //Validate user role

                    var baseAddress = authorizationManagerOptions.ValidateUserRoleEndpoint;

                    var client = new HttpClient
                    {
                        BaseAddress = new Uri(baseAddress)
                    };
                    //client.SetBasicAuthentication(authorizationManagerOptions.ClientID, authorizationManagerOptions.SecretKey);
                    //var requestString = "getuserinfo?user_name={0}&action_name={1}&resource_name={2}";

                    //var url = string.Format(requestString, user_name, action_name, resource_name);

                    //var jsonString = client.GetStringAsync(url).Result;

                    //var dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonString);
                    //if (dictionary != null)
                    //{
                    //    var activeObject = dictionary.FirstOrDefault(x => x.Key == "active");
                    //    if (!Convert.ToBoolean(activeObject.Value))
                    //        return Nok();
                    //}
                    //else
                    //    return Nok();

                }
                if (context.Principal.Identity.AuthenticationType == "Basic")
                {
                    // Use basic authen to access resource
                    //IAppsHandler appDBHandler = BusinessServiceLocator.Instance.GetService<IAppsHandler>();
                    //string client_name = context.Principal.Identity.Name;
                    //Response<Apps> appValidate =  appDBHandler.GetAppsById(client_name, false);
                    //checkACL = appValidate.Status == 1; 
                }
                return Ok();
            }


            return Nok();


        }


    }

}