﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace POC.API.App_Authentication
{

    /*
    To write a custom authorization filter, derive from one of these types:

    AuthorizeAttribute
     * Extend this class to perform authorization logic based on the current user and the user’s roles.
     
    AuthorizationFilterAttribute
     * Extend this class to perform synchronous authorization logic that is not necessarily based on the current user or role.
     
    IAuthorizationFilter. 
     * Implement this interface to perform asynchronous authorization logic; for example, 
     * if your authorization logic makes asynchronous I/O or network calls. 
     * (If your authorization logic is CPU-bound, it is simpler to derive from AuthorizationFilterAttribute, 
     * because then you don’t need to write an asynchronous method.)
     
    */

    /// <summary>
    /// This class represent an authorization proccess apply to web API
    /// include [BasicAuthorizeAttribute] in controllers to apply authorize filtering
    /// 
    /// Author : TruongND
    /// </summary>
    public class BasicAuthorizeAttribute : AuthorizeAttribute
    {
        // This string will tell clients that this attribute is a basic authentication
        private const string BasicAuthResponseHeader = "WWW-Authenticate";
        private const string BasicAuthResponseHeaderValue = "Basic";

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            return base.IsAuthorized(actionContext);
        }

        /* This function will handler authorization process */
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            // keyValidator = new StaticApiKeyValidator();
            // If there is no action context, return 
            if (actionContext == null) return;

            // If authorization is disabled (anonymous) or request is authorized
            if (AuthorizationDisabled(actionContext) || AuthorizeRequest(actionContext.ControllerContext.Request))
                return;

            // Else we must handler this unauthorized request
            this.HandleUnauthorizedRequest(actionContext);
        }

        /* This function will check if this request is valid by checking username or token keys */
        private bool AuthorizeRequest(HttpRequestMessage request)
        {
            AuthenticationHeaderValue authValue = request.Headers.Authorization;
            if (authValue == null || String.IsNullOrWhiteSpace(authValue.Parameter)
                || String.IsNullOrWhiteSpace(authValue.Scheme)
                || authValue.Scheme != BasicAuthResponseHeaderValue)
            {
                return false;
            }

            string authorizationKey = authValue.Parameter;

            return (authValue.Scheme == "Basic" && authorizationKey == ConfigurationManager.AppSettings["api:Token"]);

            //string key = authValue.Parameter;

            //IPrincipal principal = null;

            //principal = new CustomPrincipal(key);

            //HttpContext.Current.User = principal;

            //return CheckRoles(principal) && CheckUsers(principal);

            //return true;
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (actionContext == null)
                return;
            actionContext.Response = CreateUnauthorizedResponse(actionContext
                .ControllerContext.Request);
        }

        /* This is what we do with unauthorized requests */
        private HttpResponseMessage CreateUnauthorizedResponse(HttpRequestMessage request)
        {
            // Create new response with 401 : Unauthorized
            var result = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Unauthorized,
                Content = new ObjectContent<string>("Unauthorized",  GlobalConfiguration.Configuration.Formatters.JsonFormatter, "application/json")
            };

            //we need to include WWW-Authenticate header in our response,
            //so our client knows we are using HTTP authentication
            result.Headers.Add(BasicAuthResponseHeader, BasicAuthResponseHeaderValue);

            return result;
        }

        private static bool AuthorizationDisabled(HttpActionContext actionContext)
        {
            //support new AllowAnonymousAttribute
            if (!actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any())
                return actionContext.ControllerContext
                    .ControllerDescriptor
                    .GetCustomAttributes<AllowAnonymousAttribute>().Any();
            else
                return true;
        }

        private bool CheckUsers(IPrincipal principal)
        {
            string[] users = UsersSplit;
            if (users.Length == 0) return true;
            //NOTE: This is a case sensitive comparison
            return users.Any(u => principal.Identity.Name == u);
        }

        private bool CheckRoles(IPrincipal principal)
        {
            string[] roles = RolesSplit;
            if (roles.Length == 0) return true;

            // this can convert into method group
            // var t = roles.Any(principal.IsInRole);

            var t = roles.Any(x => principal.IsInRole(x));

            return t;
        }

        private string[] ParseAuthorizationHeader(string authHeader)
        {
            //string[] credentials = Encoding.ASCII.GetString(Convert
            //                                                .FromBase64String(authHeader))
            //                                                .Split(
            //                                                new[] { ':' });

            string[] credentials = { "truongnd", "min" };


            if (credentials.Length != 2 || string.IsNullOrEmpty(credentials[0])
                || string.IsNullOrEmpty(credentials[1])) return null;
            return credentials;
        }

        protected string[] RolesSplit
        {
            get { return SplitStrings(Roles); }
        }

        protected string[] UsersSplit
        {
            get { return SplitStrings(Users); }
        }

        protected static string[] SplitStrings(string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return new string[0];
            var result = input.Split(',')
                .Where(s => !String.IsNullOrWhiteSpace(s.Trim()));
            return result.Select(s => s.Trim()).ToArray();
        }


    }


    /* 
     * AuthorizationFilterAttribute
     * Extend this class to perform synchronous authorization logic that is not necessarily based on the current user or role.
     * E.g : require https
     */
    public class BasicAuthorizeFilterAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);
        }

        public override System.Threading.Tasks.Task OnAuthorizationAsync(HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken)
        {
            return base.OnAuthorizationAsync(actionContext, cancellationToken);
        }
    }

    public class CustomPrincipal : IPrincipal
    {

        private IIdentity identity;
        // private IApiKeyValidator keyValidator;

        public CustomPrincipal(string username)
        {
            // keyValidator = new StaticApiKeyValidator();

            identity = new CustomIdentity(username);

        }
        public bool IsInRole(string role)
        {
            // var cRole = role.GetRole(identity.Name);
            var cRole = string.Empty;

            if (cRole == role) return true;

            else return false;
        }

        public IIdentity Identity
        {
            get { return identity; }
        }
    }

    public class CustomIdentity : IIdentity
    {
        private string _name;
        private string _password;

        public CustomIdentity(string name)
        {
            this._name = name;
            // this._password = password;
        }
        public string Name
        {
            get { return this._name; }
        }

        public string AuthenticationType
        {
            get { return "Savis Custom Authentication"; }
        }

        public bool IsAuthenticated
        {
            // check if the user is authenticated
            get { return true; }
        }
    }
}