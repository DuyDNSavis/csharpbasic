﻿using POC.Business.Config;
using POC.Business.Logic;
using POC.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POC.Business
{
    public class UserCollection
    {
        private ILogService logger;
        private UserHandler handler;
        private HashSet<UserModel> users;

        private static readonly UserCollection _instance = new UserCollection();
        public static UserCollection Instance { get { return _instance; } }


        protected UserCollection()
        {
            handler = new UserHandler();
            logger = BusinessServiceLocator.Instance.GetService<ILogService>();
            LoadUsersToHashset();
        }

        private void LoadUsersToHashset()
        {
            users = new HashSet<UserModel>();

            // Query to list
            var listResponse = handler.QueryUsers();

            // Add to hashset

            foreach (var userModel in listResponse.Data)
            {
                users.Add(userModel);
            }
        }

        public UserModel GetUserById(Guid userId)
        {
            var user = users.FirstOrDefault(u => u.UserId == userId);
            return user;
        }
    }
}
