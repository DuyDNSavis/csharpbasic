﻿using POC.Business.Config;
using POC.Common;
using POC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC.Business.Logic
{
    public class UserHandler
    {
        ILogService logger = BusinessServiceLocator.Instance.GetService<ILogService>();


        public Response<List<UserModel>> QueryUsers()
        {
            try
            {
                var result = new Response<List<UserModel>>(1, string.Empty, new List<UserModel>());

                using (var unitOfWork = new UnitOfWork())
                {
                    var data = from u in unitOfWork.GetRepository<aspnet_Users>().GetAll()
                               select new { User = u };

                    
                    // Execute query
                    var executedResult = data.ToList();

                    foreach (var item in executedResult)
	                {
		                // Convert
                        var user = new UserModel();
                        user.FullName = item.User.FullName;
                        user.UserId = item.User.UserId;
                        user.UserName = item.User.UserName;

                        result.Data.Add(user);

	                }
                    
                    // Return result
                    return result;
                }

                //return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<List<UserModel>>(-1, ex.Message, null);
            }

        }


    }
}
