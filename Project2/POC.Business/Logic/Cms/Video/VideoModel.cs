﻿using POC.Business.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace POC.Business
{

    public class VideoBaseModel
    {
        public Guid VideoId { get; set; }
        public string Title { get; set; }
    }

    public class VideoHistory
    {
        public long VideoHistoryId { get; set; }
        public Guid VideoId { get; set; }
        public string Action { get; set; }
        public UserBaseModel CreatedByUser { get; set; }
        public DateTime CreateOnDate { get; set; }
    }

    public class VideoAttribute
    {
        public long VideoAttributeId { get; set; }
        public Guid VideoId { get; set; }


    }

    public class VideoProfile
    {
        public Guid VideoProfileId { get; set; }
        public Guid VideoId { get; set; }
        public string Name { get; set; }
        public string Resolution { get; set; }
        public string AspectRatio { get; set; }
        public int Duration { get; set; }
        public string PhysicalFilePath { get; set; }
        public string VideoCodec { get; set; }
        public Guid CreatedByUserId { get; set; }

        public bool IsDefaultProfile { get; set; }
        public int Order { get; set; }
    }

    public class VideoProfileCreateRequestModel
    {
        public string Name { get; set; }
        public string Resolution { get; set; }
        public string AspectRatio { get; set; }
        public int Duration { get; set; }
        public string PhysicalFilePath { get; set; }
        public string VideoCodec { get; set; }
        public Guid CreatedByUserId { get; set; }
        public int Order { get; set; }

        public VideoProfileCreateRequestModel()
        {
            Order = 0;
        }
    }
    public class VideoProfileUpdateRequestModel
    {
        public Guid VideoProfileId { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
        public string PhysicalFilePath { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public int Order { get; set; }
    }
    public class VideoModel : VideoBaseModel
    {
        public string Description { get; set; }
        public string ThumbnailPath { get; set; }
        public string ThumbnailDescription { get; set; }
        public int Duration { get; set; }
        public int DisplayStatus { get; set; }
        public string DurationInText { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public UserBaseModel CreatedByUser { get; set; }
        public UserBaseModel LastModifiedByUser { get; set; }

        public int TotalViewTime { get; set; }
        public int MobileViewTime { get; set; }
        public int ProfileCount { get; set; }
        public string WorkflowCode { get; set; }
        public string WorkflowState { get; set; }
        public int DesktopViewCount { get; set; }
        public int MobileViewCount { get; set; }
        public List<VideoHistory> Histories { get; set; }
        public List<VideoProfile> Profiles { get; set; }
    }

    public class VideoQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public int? DisplayStatus { get; set; }
        public bool? IncludeVideoHistory { get; set; }
        public bool? IncludeVideoProfile { get; set; }
        public VideoQueryFilter()
        {
            PageNumber = 1;
            PageSize = 10;
        }
    }

    // Create request model
    public class VideoCreateRequestModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ThumbnailPath { get; set; }
        public string ThumbnailDescription { get; set; }
        public int Duration { get; set; }
        public string StreamingProtocol { get; set; }
        public string WorkflowCode { get; set; }
        public Guid CreatedByUserId { get; set; }
        public Guid ApplicationId { get; set; }
        public VideoProfileCreateRequestModel VideoProfile { get; set; }
    }

    public class VideoUpdateRequestModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public bool ThumbnailAvailable { get; set; }
        public Guid ThumbnailMdmFileId { get; set; }
        public string ThumbnailDescription { get; set; }
        public int Duration { get; set; }
        public int DesktopViewCount { get; set; }
        public int MobileViewCount { get; set; }
        public string WorkflowCode { get; set; }
        public int? DisplayStatus { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public VideoProfileUpdateRequestModel VideoProfile { get; set; }
    }
    public class VideoDeleteResponseModel
    {
        public Guid VideoId { get; set; }
        public string Title { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }

    }
}
