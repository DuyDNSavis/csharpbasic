﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using POC.Common;

namespace POC.Business
{
    public interface IVideoHandler
    {
        Response<VideoModel> GetVideoById(Guid videoId);
        Response<VideoModel> CreateVideo(VideoCreateRequestModel video);
        Response<VideoModel> UpdateVideo(Guid videoId, VideoUpdateRequestModel video);
        Response<VideoDeleteResponseModel> DeleteVideo(Guid videoId);
        Response<VideoProfile> GetVideoProfileByVideoId(Guid videoId);
        Response<VideoProfile> GetVideoProfileById(Guid videoId, Guid videoProfileId);
        Response<VideoProfile> CreateVideoProfileByVideoId(Guid videoId, VideoProfileCreateRequestModel videoProfile);
        Response<IList<VideoModel>> GetFilter(VideoQueryFilter filter);
        Response<IList<VideoProfile>> GetVideoProfilesByVideoId(Guid videoId);
        Response<VideoProfile> DeleteVideoProfile(Guid videoId, Guid videoProfileId);
        Response<VideoModel> UpdateVideoTotalView(Guid videoId);
        Response<VideoModel> GetByTitle(string title);
    }
}