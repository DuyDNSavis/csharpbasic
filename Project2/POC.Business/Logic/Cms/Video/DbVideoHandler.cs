﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using POC.Business.Config;
using POC.Common;
using POC.Data;
using WMPLib;

namespace POC.Business
{
    public class DbVideoHandler : IVideoHandler
    {
        ILogService logger = BusinessServiceLocator.Instance.GetService<ILogService>();

        #region GET

        public Response<VideoModel> GetVideoById(Guid videoId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var video = unitOfWork.GetRepository<cms_Video>().GetById(videoId);
                    return new Response<VideoModel>(ConfigType.SUCCESS, "OK", ConvertVideo(video));
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<VideoModel>(ConfigType.ERROR, ex.Message, null);
            }
        }

        public Response<VideoProfile> GetVideoProfileByVideoId(Guid videoId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var videoProfile =
                        unitOfWork.GetRepository<cms_Video_Profile>().Get(m => m.VideoId.Equals(videoId));
                    return new Response<VideoProfile>(ConfigType.SUCCESS, "OK", ConvertVideoProfile(videoProfile));
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<VideoProfile>(ConfigType.ERROR, ex.Message, null);
            }
        }

        public Response<IList<VideoModel>> GetFilter(VideoQueryFilter filter)
        {
            try
            {
                string textSearch = filter.TextSearch;
                using (var unitOfWork = new UnitOfWork())
                {
                    var data = from v in unitOfWork.GetRepository<cms_Video>().GetAll()
                               join at in unitOfWork.GetRepository<cms_Video_Attribute>().GetAll() on v.VideoId equals
                                   at.VideoId
                               join prf in unitOfWork.GetRepository<cms_Video_Profile>().GetAll() on v.VideoId equals
                                   prf.VideoId
                               where prf.IsDefaultProfile == true
                               select new { Video = v, Attribute = at, profile = prf };
                    if (!string.IsNullOrEmpty(textSearch))
                    {
                        data = data.Where(m => m.Video.Title.ToLower().Contains(textSearch.ToLower()));
                    }
                    if (filter.DisplayStatus.HasValue)
                    {
                        data = data.Where(m => m.Video.DisplayStatus == filter.DisplayStatus.Value);
                    }
                    var count = data.Count();
                    if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                    {
                        if (filter.PageSize.Value <= 0) filter.PageSize = 10;
                        int excludedRows = (filter.PageNumber.Value - 1) * (filter.PageSize.Value - 1);
                        if (excludedRows <= 0) excludedRows = 0;
                        data =
                            data.OrderByDescending(m => m.Video.CreatedOnDate).Skip(excludedRows).Take(filter.PageSize.Value + 1);
                    }
                    List<VideoModel> videoModels = new List<VideoModel>();
                    foreach (var item in data)
                    {
                        videoModels.Add(ConvertVideo(item.Video, item.Attribute, item.profile));
                    }
                    Response<IList<VideoModel>> response = new Response<IList<VideoModel>>(ConfigType.SUCCESS, "OK",
                        videoModels)
                    {
                        DataCount = count
                    };
                    return response;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<IList<VideoModel>>(ConfigType.ERROR, ex.Message, null);
            }
        }

        public Response<IList<VideoProfile>> GetVideoProfilesByVideoId(Guid videoId)
        {
            try
            {

                using (var unitOfWork = new UnitOfWork())
                {
                    var data = unitOfWork.GetRepository<cms_Video_Profile>().GetMany(m => m.VideoId == videoId);

                    var count = data.Count();

                    List<VideoProfile> videoProfileModels = new List<VideoProfile>();

                    foreach (var item in data)
                    {
                        videoProfileModels.Add(ConvertVideoProfile(item));
                    }

                    Response<IList<VideoProfile>> response =
                        new Response<IList<VideoProfile>>(ConfigType.SUCCESS, "OK", videoProfileModels)
                        {
                            DataCount = count
                        };

                    return response;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<IList<VideoProfile>>(ConfigType.ERROR, ex.Message, null);
            }
        }
        public Response<VideoModel> GetByTitle(string title)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var model = unitOfWork.GetRepository<cms_Video>().Get(m => m.Title.ToLower().Equals(title.ToLower()));
                    if (model != null)
                    {
                        return new Response<VideoModel>(5, "Tiêu đề đã tồn tại", ConvertVideo(model));
                    }
                    else
                    {
                        return new Response<VideoModel>(ConfigType.SUCCESS, "NO DATA", null);
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<VideoModel>(ConfigType.ERROR, ex.Message, null);
            }
        }
        #endregion

        #region ADD,UPDATA,DELETE

        public Response<VideoModel> CreateVideo(VideoCreateRequestModel model)
        {
            try
            {

                using (var unitOfWork = new UnitOfWork())
                {
                    var db = unitOfWork.DataContext;
                    //create new video
                    var newVideo = new cms_Video();

                    // Convert video
                    newVideo.Duration = model.Duration;
                    newVideo.DurationInText = new TimeSpan(0, 0, model.Duration).ToString(@"mm\:ss");

                    // Mandatory fields of a video
                    newVideo.VideoId = Guid.NewGuid();
                    newVideo.Title = model.Title;
                    newVideo.Description = model.Description;
                    newVideo.PublishedOnDate = DateTime.Now;
                    newVideo.ThumbnailPath = model.ThumbnailPath;
                    newVideo.ThumbnailDescription = model.ThumbnailDescription;
                    newVideo.ThumbnailAvailable = true;
                    newVideo.DisplayStatus = ConfigType.VIDEO_DISPLAY_SHOW;

                    // System fields of a video
                    newVideo.CreatedByUserId = model.CreatedByUserId;
                    newVideo.PublishedByUserId = model.CreatedByUserId;
                    newVideo.CreatedOnDate = DateTime.Now;
                    newVideo.LastModifiedOnDate = DateTime.Now;
                    newVideo.LastModifiedByUserId = model.CreatedByUserId;
                    newVideo.ApplicationId = model.ApplicationId;

                    // Add video attribute
                    var newVideoAttri = new cms_Video_Attribute
                    {
                        VideoId = newVideo.VideoId,
                        MobileViewTime = 0,
                        TotalViewTime = 0,
                        DesktopViewCount = 0,
                        MobileViewCount = 0,
                        // By default we have one profile
                        ProfileCount = 1,
                        WorkflowCode = model.WorkflowCode,
                    };

                    // Add video profile 
                    if (model.VideoProfile != null)
                    {
                        var newVideoProfile = new cms_Video_Profile()
                        {
                            VideoProfileId = Guid.NewGuid(),
                            VideoId = newVideo.VideoId,
                            Name = "default",
                            AspectRatio = model.VideoProfile.AspectRatio,
                            CreatedByUserId = model.VideoProfile.CreatedByUserId,
                            CreatedOnDate = DateTime.Now,
                            Duration = newVideo.Duration,
                            DurationInText = newVideo.DurationInText,
                            IsDefaultProfile = true,
                            Resolution = model.VideoProfile.Resolution,
                            VideoCodec = model.VideoProfile.VideoCodec,
                            PhysicalFilePath = model.VideoProfile.PhysicalFilePath,
                            Order = model.VideoProfile.Order
                        };
                        newVideo.cms_Video_Profile.Add(newVideoProfile);
                    }
                    // Add all
                    newVideo.cms_Video_Attribute.Add(newVideoAttri);
                    db.cms_Video.Add(newVideo);

                    var commitResult = unitOfWork.Save();

                    if (commitResult >= ConfigType.SUCCESS)
                    {
                        // Insert history
                        var checkMaking = CreateVideoHistory(newVideo.VideoId, ConfigType.ACION_CREATE,
                            newVideo.CreatedByUserId);

                        return new Response<VideoModel>(ConfigType.SUCCESS, "OK", ConvertVideo(newVideo));
                    }
                    else
                    {
                        return new Response<VideoModel>(ConfigType.NODATA, "NOT OK", null);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<VideoModel>(ConfigType.ERROR, ex.Message, null);
            }
        }


        public Response<VideoModel> UpdateVideo(Guid videoId, VideoUpdateRequestModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var existingVideo = unitOfWork.GetRepository<cms_Video>().Get(m => m.VideoId == videoId);
                    var videoProfile = unitOfWork.GetRepository<cms_Video_Profile>().Get(m => m.VideoId == videoId && m.VideoProfileId == model.VideoProfile.VideoProfileId);
                    //read video
                    var player = new WindowsMediaPlayer();
                    var clip = player.newMedia(ConfigurationManager.AppSettings["folder:RootPath"] + model.VideoProfile.PhysicalFilePath);
                    existingVideo.Duration = (int)Math.Round(clip.duration, 0);
                    existingVideo.DurationInText = clip.durationString;
                    //end read

                    existingVideo.LastModifiedByUserId = model.LastModifiedByUserId;
                    existingVideo.Title = model.Title;
                    existingVideo.Description = model.Description;
                    existingVideo.ThumbnailAvailable = model.ThumbnailAvailable;
                    existingVideo.ThumbnailDescription = model.ThumbnailDescription;
                    if (model.DisplayStatus != null) existingVideo.DisplayStatus = model.DisplayStatus.Value;

                    // Update video attribute table
                    var videoAttri = existingVideo.cms_Video_Attribute.FirstOrDefault(m => m.VideoId == videoId);
                    videoAttri.WorkflowCode = model.WorkflowCode;
                    videoAttri.DesktopViewCount = model.DesktopViewCount;
                    videoAttri.MobileViewCount = model.MobileViewCount;

                    // Update video profile table
                    videoProfile.Duration = existingVideo.Duration;
                    videoProfile.DurationInText = existingVideo.DurationInText;
                    videoProfile.PhysicalFilePath = model.VideoProfile.PhysicalFilePath;
                    videoProfile.Order = model.VideoProfile.Order;

                    // Update to repository
                    unitOfWork.GetRepository<cms_Video>().Update(existingVideo);
                    unitOfWork.GetRepository<cms_Video_Profile>().Update(videoProfile);

                    var commitResult = unitOfWork.Save();
                    if (commitResult >= ConfigType.SUCCESS)
                    {
                        // Insert history
                        var addHistoryCommand = CreateVideoHistory(existingVideo.VideoId, ConfigType.ACION_UPDATE,
                            existingVideo.CreatedByUserId);
                        return new Response<VideoModel>(ConfigType.SUCCESS, "OK", ConvertVideo(existingVideo));
                    }
                    else
                    {
                        return new Response<VideoModel>(ConfigType.ERROR, "NOT OK", null);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<VideoModel>(ConfigType.ERROR, ex.Message, null);
            }
        }

        public Response<VideoDeleteResponseModel> DeleteVideo(Guid videoId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var db = unitOfWork.DataContext;
                    var modelVideo = unitOfWork.GetRepository<cms_Video>().GetById(videoId);
                    if (modelVideo != null)
                    {
                        var modelVideoHistorys = db.cms_Video_History.Where(m => m.VideoId.Equals(videoId));
                        var modelVideocAttribute =
                            unitOfWork.GetRepository<cms_Video_Attribute>()
                                .Get(m => m.VideoId.Equals(videoId));
                        var modelVideoProfile =
                            unitOfWork.GetRepository<cms_Video_Profile>().Get(m => m.VideoId.Equals(videoId));

                        // if video history record exists, delete it
                        if (modelVideoHistorys.Any())
                        {
                            db.cms_Video_History.RemoveRange(modelVideoHistorys);
                        }

                        // if video attribute record exists, delete it
                        if (modelVideocAttribute != null)
                        {
                            unitOfWork.GetRepository<cms_Video_Attribute>().Delete(modelVideocAttribute);
                        }

                        // if video profile record exists, delete it
                        if (modelVideoProfile != null)
                        {
                            unitOfWork.GetRepository<cms_Video_Profile>().Delete(modelVideoProfile);
                        }

                        // then finally delete video item
                        unitOfWork.GetRepository<cms_Video>().Delete(modelVideo);

                        // Commit changes to database
                        var commitResult = unitOfWork.Save();

                        if (commitResult >= ConfigType.NODATA)
                        {
                            return new Response<VideoDeleteResponseModel>(ConfigType.SUCCESS, "Succesfully deleted video with id = " + videoId, null);
                        }
                        else
                        {
                            return new Response<VideoDeleteResponseModel>(ConfigType.ERROR, "Unable to delete video with id = " + videoId, null);
                        }
                    }
                    else
                    {
                        return new Response<VideoDeleteResponseModel>(ConfigType.ERROR, "Unable to delete video with id = " + videoId, null);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<VideoDeleteResponseModel>(ConfigType.ERROR, ex.Message, null);
            }
        }


        public bool CreateVideoHistory(Guid videoGuid, string action, Guid createByUserId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var videoHistory = new cms_Video_History()
                    {
                        VideoId = videoGuid,
                        Action = action,
                        CreatedByUserId = createByUserId,
                        CreatedOnDate = DateTime.Now
                    };
                    unitOfWork.GetRepository<cms_Video_History>().Add(videoHistory);
                    if (unitOfWork.Save() >= ConfigType.SUCCESS)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;
            }
        }

        public Response<VideoProfile> GetVideoProfileById(Guid videoId, Guid videoProfileId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var videoProfile =
                        unitOfWork.GetRepository<cms_Video_Profile>()
                            .Get(m => m.VideoProfileId == videoProfileId && m.VideoId == videoId);
                    return new Response<VideoProfile>(ConfigType.SUCCESS, "OK", ConvertVideoProfile(videoProfile));
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<VideoProfile>(ConfigType.ERROR, ex.Message, null);
            }
        }

        public Response<VideoProfile> CreateVideoProfileByVideoId(Guid videoId,
            VideoProfileCreateRequestModel videoProfile)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var newVideoProfile = new cms_Video_Profile();
                    newVideoProfile.VideoId = videoId;
                    newVideoProfile.Duration = videoProfile.Duration;
                    newVideoProfile.IsDefaultProfile = true;
                    newVideoProfile.Name = videoProfile.Name;
                    newVideoProfile.Order = videoProfile.Order;
                    newVideoProfile.PhysicalFilePath = videoProfile.PhysicalFilePath;
                    newVideoProfile.Resolution = videoProfile.Resolution;
                    newVideoProfile.VideoCodec = videoProfile.VideoCodec;
                    newVideoProfile.VideoProfileId = Guid.NewGuid();
                    newVideoProfile.AspectRatio = videoProfile.AspectRatio;
                    newVideoProfile.CreatedByUserId = videoProfile.CreatedByUserId;
                    newVideoProfile.CreatedOnDate = DateTime.Now;

                    TimeSpan time = TimeSpan.FromMilliseconds(videoProfile.Duration);
                    string str = time.ToString(@"hh\:mm\:ss");
                    newVideoProfile.DurationInText = str;

                    // Add video profile
                    unitOfWork.GetRepository<cms_Video_Profile>().Add(newVideoProfile);

                    // Insert video profile count
                    var videoAtt = unitOfWork.GetRepository<cms_Video_Attribute>().Get(m => m.VideoId == videoId);
                    videoAtt.ProfileCount += 1;
                    unitOfWork.GetRepository<cms_Video_Attribute>().Update(videoAtt);

                    if (unitOfWork.Save() >= ConfigType.SUCCESS)
                    {
                        return new Response<VideoProfile>(ConfigType.SUCCESS, "OK",
                            ConvertVideoProfile(newVideoProfile));
                    }
                    else
                    {
                        return new Response<VideoProfile>(ConfigType.NODATA, "NO DATA", null);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new Response<VideoProfile>(ConfigType.ERROR, ex.Message, null);
            }
        }


        public Response<VideoModel> UpdateVideoTotalView(Guid videoId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var model = unitOfWork.GetRepository<cms_Video_Attribute>().Get(m => m.VideoId == videoId);
                    model.TotalViewTime += 1;
                    if (unitOfWork.Save() >= ConfigType.SUCCESS)
                    {
                        return new Response<VideoModel>(ConfigType.SUCCESS, "OK", ConvertVideo(model.cms_Video, model, null));
                    }
                    return new Response<VideoModel>(ConfigType.NODATA, "NOT OK", null);
                }
            }
            catch (Exception ex)
            {
                return new Response<VideoModel>(ConfigType.ERROR, ex.Message, null);
            }
        }

        public Response<VideoProfile> DeleteVideoProfile(Guid videoId, Guid videoProfileId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var videoProfile =
                        unitOfWork.GetRepository<cms_Video_Profile>()
                            .Get(m => m.VideoId == videoId && m.VideoProfileId == videoProfileId);
                    unitOfWork.GetRepository<cms_Video_Profile>().Delete(videoProfile);
                    //update video profile count
                    var videoAtt = unitOfWork.GetRepository<cms_Video_Attribute>().Get(m => m.VideoId == videoId);
                    videoAtt.ProfileCount -= 1;
                    if (unitOfWork.Save() >= ConfigType.SUCCESS)
                    {
                        return new Response<VideoProfile>(ConfigType.SUCCESS, "OK", null);
                    }
                    return new Response<VideoProfile>(ConfigType.NODATA, "NOT OK", null);
                }
            }
            catch (Exception ex)
            {
                return new Response<VideoProfile>(ConfigType.ERROR, ex.Message, null);
            }
        }

        #endregion

        #region CONVERT DATA

        public VideoModel ConvertVideo(cms_Video cmsVideo)
        {
            try
            {
                return ConvertVideo(cmsVideo, null, null);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new VideoModel();
            }
        }

        public VideoModel ConvertVideo(cms_Video cmsVideo, cms_Video_Attribute cmsVideoAttribute, cms_Video_Profile cmsVideoProfile)
        {
            try
            {
                VideoModel model = new VideoModel();
                model.VideoId = cmsVideo.VideoId;
                model.CreatedOnDate = cmsVideo.CreatedOnDate;
                model.DisplayStatus = cmsVideo.DisplayStatus;
                model.LastModifiedOnDate = cmsVideo.LastModifiedOnDate;
                model.Description = cmsVideo.Description;
                model.Duration = cmsVideo.Duration;
                model.DurationInText = cmsVideo.DurationInText;
                model.ThumbnailPath = cmsVideo.ThumbnailPath;
                model.ThumbnailDescription = cmsVideo.ThumbnailDescription;
                model.Title = cmsVideo.Title;

                // Fill user from cached collection
                model.CreatedByUser = UserCollection.Instance.GetUserById(cmsVideo.CreatedByUserId);
                model.LastModifiedByUser = UserCollection.Instance.GetUserById(cmsVideo.LastModifiedByUserId);


                // Fill attributes
                if (cmsVideoAttribute != null)
                {
                    model.MobileViewTime = cmsVideoAttribute.MobileViewTime;
                    model.TotalViewTime = cmsVideoAttribute.TotalViewTime;
                    model.WorkflowCode = cmsVideoAttribute.WorkflowCode;
                    model.WorkflowState = cmsVideoAttribute.WorkflowState;
                    model.DesktopViewCount = cmsVideoAttribute.DesktopViewCount;
                    model.MobileViewCount = cmsVideoAttribute.MobileViewCount;
                    model.ProfileCount = cmsVideoAttribute.ProfileCount;
                }

                // Fill video profile
                if (cmsVideoProfile != null)
                {
                    model.Profiles = new List<VideoProfile>();
                    model.Profiles.Add(ConvertVideoProfile(cmsVideoProfile));
                }
                return model;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new VideoModel();
            }
        }

        public VideoProfile ConvertVideoProfile(cms_Video_Profile profileEntity)
        {
            try
            {
                var model = new VideoProfile
                {
                    VideoId = profileEntity.VideoId,
                    VideoProfileId = profileEntity.VideoProfileId,
                    Name = profileEntity.Name,
                    Order = profileEntity.Order,
                    AspectRatio = profileEntity.AspectRatio,
                    Duration = profileEntity.Duration,
                    IsDefaultProfile = profileEntity.IsDefaultProfile,
                    PhysicalFilePath = profileEntity.PhysicalFilePath,
                    Resolution = profileEntity.Resolution,
                    VideoCodec = profileEntity.VideoCodec,
                    CreatedByUserId = profileEntity.CreatedByUserId,
                };

                return model;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new VideoProfile();
            }
        }

        public List<VideoProfile> ConvertVideoProfiles(List<cms_Video_Profile> profileEntities)
        {
            try
            {
                return profileEntities.Select(ConvertVideoProfile).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new List<VideoProfile>();
            }
        }

        public VideoHistory ConvertVideoHistory(cms_Video_History dataVideoHistory)
        {
            try
            {
                var model = new VideoHistory
                {
                    Action = dataVideoHistory.Action,
                    CreatedByUser = UserCollection.Instance.GetUserById(dataVideoHistory.CreatedByUserId),
                    CreateOnDate = dataVideoHistory.CreatedOnDate,
                    VideoHistoryId = dataVideoHistory.VideoHistoryId,
                    VideoId = dataVideoHistory.VideoId
                };
                return model;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new VideoHistory();
            }
        }

        public List<VideoHistory> ConvertVideoHistorys(List<cms_Video_History> dataVideoHistories)
        {
            try
            {
                return dataVideoHistories.Select(ConvertVideoHistory).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new List<VideoHistory>();
            }
        }



        #endregion
    }
}