﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC.Business.Config
{
    public static class ConfigType
    {
        public const int STATUS_OK = 1;
        public const int STATUS_NOTOK = 0;

        public const int STATUS_ACTIVE = 1;
        public const int STATUS_DEACTIVE = 0;

        public const int SUCCESS = 1;
        public const int ERROR = -1;
        public const int NODATA = 0;
        public const int DATA_IS_EXIST = 2;

        public const int MENU_STATUS_ACTIVE = 1;
        public const int MENU_STATUS_DEACTIVE = 1;

        public const int VIDEO_DISPLAY_SHOW = 1;
        public const int VIDEO_DISPLAY_HIDE = 0;

        public const string ACION_CREATE = "CREATE";
        public const string ACION_UPDATE = "UPDATE";


        public const int DISPLAY_ACTIVE = 1;
        public const int DISPLAY_DEACTIVE = 0;

        //get random password
        public static string RandomPassword(int numericLength, int lCaseLength, int uCaseLength, int specialLength)
        {
            Random random = new Random();

            //char set random
            string PASSWORD_CHARS_LCASE = "abcdefgijkmnopqrstwxyz";
            string PASSWORD_CHARS_UCASE = "ABCDEFGHJKLMNPQRSTWXYZ";
            string PASSWORD_CHARS_NUMERIC = "1234567890";
            string PASSWORD_CHARS_SPECIAL = "!@#$%^&*()-+<>?";
            if ((numericLength + lCaseLength + uCaseLength + specialLength) < 8)
                return string.Empty;
            else
            {
                //get char
                var strNumeric = new string(Enumerable.Repeat(PASSWORD_CHARS_NUMERIC, numericLength)
                    .Select(s => s[random.Next(s.Length)]).ToArray());

                var strUper = new string(Enumerable.Repeat(PASSWORD_CHARS_UCASE, uCaseLength)
                    .Select(s => s[random.Next(s.Length)]).ToArray());

                var strLower = new string(Enumerable.Repeat(PASSWORD_CHARS_LCASE, lCaseLength)
                    .Select(s => s[random.Next(s.Length)]).ToArray());

                var strSpecial = new string(Enumerable.Repeat(PASSWORD_CHARS_SPECIAL, specialLength)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
                //result : ký tự số + chữ hoa + chữ thường + các ký tự đặc biệt > 8
                var strResult = strNumeric + strUper + strLower + strSpecial;
                return strResult;
            }
        }
    }
}
