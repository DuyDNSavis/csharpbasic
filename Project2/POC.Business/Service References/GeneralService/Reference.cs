﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POC.Business.GeneralService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ArchiveFileResult", Namespace="http://schemas.datacontract.org/2004/07/CMS.GeneralService.Business")]
    [System.SerializableAttribute()]
    public partial class ArchiveFileResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FileIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FilePathField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsSuccessField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double QueueIdField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FileId {
            get {
                return this.FileIdField;
            }
            set {
                if ((object.ReferenceEquals(this.FileIdField, value) != true)) {
                    this.FileIdField = value;
                    this.RaisePropertyChanged("FileId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FilePath {
            get {
                return this.FilePathField;
            }
            set {
                if ((object.ReferenceEquals(this.FilePathField, value) != true)) {
                    this.FilePathField = value;
                    this.RaisePropertyChanged("FilePath");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsSuccess {
            get {
                return this.IsSuccessField;
            }
            set {
                if ((this.IsSuccessField.Equals(value) != true)) {
                    this.IsSuccessField = value;
                    this.RaisePropertyChanged("IsSuccess");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double QueueId {
            get {
                return this.QueueIdField;
            }
            set {
                if ((this.QueueIdField.Equals(value) != true)) {
                    this.QueueIdField = value;
                    this.RaisePropertyChanged("QueueId");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ArchiveFileStatus", Namespace="http://schemas.datacontract.org/2004/07/CMS.GeneralService.Business")]
    [System.SerializableAttribute()]
    public partial class ArchiveFileStatus : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double AverageSpeedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.TimeSpan TimeLeftField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double AverageSpeed {
            get {
                return this.AverageSpeedField;
            }
            set {
                if ((this.AverageSpeedField.Equals(value) != true)) {
                    this.AverageSpeedField = value;
                    this.RaisePropertyChanged("AverageSpeed");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Status {
            get {
                return this.StatusField;
            }
            set {
                if ((object.ReferenceEquals(this.StatusField, value) != true)) {
                    this.StatusField = value;
                    this.RaisePropertyChanged("Status");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.TimeSpan TimeLeft {
            get {
                return this.TimeLeftField;
            }
            set {
                if ((this.TimeLeftField.Equals(value) != true)) {
                    this.TimeLeftField = value;
                    this.RaisePropertyChanged("TimeLeft");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CopyFileResult", Namespace="http://schemas.datacontract.org/2004/07/CMS.GeneralService.Business")]
    [System.SerializableAttribute()]
    public partial class CopyFileResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FileIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FilePathField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsSuccessField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double QueueIdField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FileId {
            get {
                return this.FileIdField;
            }
            set {
                if ((object.ReferenceEquals(this.FileIdField, value) != true)) {
                    this.FileIdField = value;
                    this.RaisePropertyChanged("FileId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FilePath {
            get {
                return this.FilePathField;
            }
            set {
                if ((object.ReferenceEquals(this.FilePathField, value) != true)) {
                    this.FilePathField = value;
                    this.RaisePropertyChanged("FilePath");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsSuccess {
            get {
                return this.IsSuccessField;
            }
            set {
                if ((this.IsSuccessField.Equals(value) != true)) {
                    this.IsSuccessField = value;
                    this.RaisePropertyChanged("IsSuccess");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double QueueId {
            get {
                return this.QueueIdField;
            }
            set {
                if ((this.QueueIdField.Equals(value) != true)) {
                    this.QueueIdField = value;
                    this.RaisePropertyChanged("QueueId");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CopyFileStatus", Namespace="http://schemas.datacontract.org/2004/07/CMS.GeneralService.Business")]
    [System.SerializableAttribute()]
    public partial class CopyFileStatus : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double AverageSpeedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.TimeSpan TimeLeftField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double AverageSpeed {
            get {
                return this.AverageSpeedField;
            }
            set {
                if ((this.AverageSpeedField.Equals(value) != true)) {
                    this.AverageSpeedField = value;
                    this.RaisePropertyChanged("AverageSpeed");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Status {
            get {
                return this.StatusField;
            }
            set {
                if ((object.ReferenceEquals(this.StatusField, value) != true)) {
                    this.StatusField = value;
                    this.RaisePropertyChanged("Status");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.TimeSpan TimeLeft {
            get {
                return this.TimeLeftField;
            }
            set {
                if ((this.TimeLeftField.Equals(value) != true)) {
                    this.TimeLeftField = value;
                    this.RaisePropertyChanged("TimeLeft");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PhysicalFolder", Namespace="http://schemas.datacontract.org/2004/07/CMS.GeneralService")]
    [System.SerializableAttribute()]
    public partial class PhysicalFolder : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ParentIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PhysicalPathField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ParentId {
            get {
                return this.ParentIdField;
            }
            set {
                if ((object.ReferenceEquals(this.ParentIdField, value) != true)) {
                    this.ParentIdField = value;
                    this.RaisePropertyChanged("ParentId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PhysicalPath {
            get {
                return this.PhysicalPathField;
            }
            set {
                if ((object.ReferenceEquals(this.PhysicalPathField, value) != true)) {
                    this.PhysicalPathField = value;
                    this.RaisePropertyChanged("PhysicalPath");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PhysicalFile", Namespace="http://schemas.datacontract.org/2004/07/CMS.GeneralService")]
    [System.SerializableAttribute()]
    public partial class PhysicalFile : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime CreatedOnDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime ModifiedOnDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PathField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double SizeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TypeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime CreatedOnDate {
            get {
                return this.CreatedOnDateField;
            }
            set {
                if ((this.CreatedOnDateField.Equals(value) != true)) {
                    this.CreatedOnDateField = value;
                    this.RaisePropertyChanged("CreatedOnDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime ModifiedOnDate {
            get {
                return this.ModifiedOnDateField;
            }
            set {
                if ((this.ModifiedOnDateField.Equals(value) != true)) {
                    this.ModifiedOnDateField = value;
                    this.RaisePropertyChanged("ModifiedOnDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Path {
            get {
                return this.PathField;
            }
            set {
                if ((object.ReferenceEquals(this.PathField, value) != true)) {
                    this.PathField = value;
                    this.RaisePropertyChanged("Path");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Size {
            get {
                return this.SizeField;
            }
            set {
                if ((this.SizeField.Equals(value) != true)) {
                    this.SizeField = value;
                    this.RaisePropertyChanged("Size");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Type {
            get {
                return this.TypeField;
            }
            set {
                if ((object.ReferenceEquals(this.TypeField, value) != true)) {
                    this.TypeField = value;
                    this.RaisePropertyChanged("Type");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://Microsoft.ServiceModel.CMS_GeneralService", ConfigurationName="GeneralService.IGeneralService")]
    public interface IGeneralService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/StartArchiveFile" +
            "", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/StartArchiveFile" +
            "Response")]
        POC.Business.GeneralService.ArchiveFileResult StartArchiveFile(string fileId, string sourcePath, string destinationPath);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/StartArchiveFile" +
            "", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/StartArchiveFile" +
            "Response")]
        System.Threading.Tasks.Task<POC.Business.GeneralService.ArchiveFileResult> StartArchiveFileAsync(string fileId, string sourcePath, string destinationPath);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetArchiveStatus" +
            "", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetArchiveStatus" +
            "Response")]
        POC.Business.GeneralService.ArchiveFileStatus GetArchiveStatus(string fileId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetArchiveStatus" +
            "", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetArchiveStatus" +
            "Response")]
        System.Threading.Tasks.Task<POC.Business.GeneralService.ArchiveFileStatus> GetArchiveStatusAsync(string fileId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/StartCopyFile", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/StartCopyFileRes" +
            "ponse")]
        POC.Business.GeneralService.CopyFileResult StartCopyFile(string fileId, string sourcePath, string uploadPath);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/StartCopyFile", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/StartCopyFileRes" +
            "ponse")]
        System.Threading.Tasks.Task<POC.Business.GeneralService.CopyFileResult> StartCopyFileAsync(string fileId, string sourcePath, string uploadPath);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetCopyStatus", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetCopyStatusRes" +
            "ponse")]
        POC.Business.GeneralService.CopyFileStatus GetCopyStatus(string fileId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetCopyStatus", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetCopyStatusRes" +
            "ponse")]
        System.Threading.Tasks.Task<POC.Business.GeneralService.CopyFileStatus> GetCopyStatusAsync(string fileId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetAllFolders", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetAllFoldersRes" +
            "ponse")]
        POC.Business.GeneralService.PhysicalFolder[] GetAllFolders();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetAllFolders", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetAllFoldersRes" +
            "ponse")]
        System.Threading.Tasks.Task<POC.Business.GeneralService.PhysicalFolder[]> GetAllFoldersAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetFiles", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetFilesResponse" +
            "")]
        POC.Business.GeneralService.PhysicalFile[] GetFiles(string path);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetFiles", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetFilesResponse" +
            "")]
        System.Threading.Tasks.Task<POC.Business.GeneralService.PhysicalFile[]> GetFilesAsync(string path);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetChilds", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetChildsRespons" +
            "e")]
        POC.Business.GeneralService.PhysicalFolder[] GetChilds(string path);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetChilds", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetChildsRespons" +
            "e")]
        System.Threading.Tasks.Task<POC.Business.GeneralService.PhysicalFolder[]> GetChildsAsync(string path);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/CreateSourceFold" +
            "er", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/CreateSourceFold" +
            "erResponse")]
        int CreateSourceFolder(string folderPath);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/CreateSourceFold" +
            "er", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/CreateSourceFold" +
            "erResponse")]
        System.Threading.Tasks.Task<int> CreateSourceFolderAsync(string folderPath);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetFileSize", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetFileSizeRespo" +
            "nse")]
        long GetFileSize(string filePath);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetFileSize", ReplyAction="http://Microsoft.ServiceModel.CMS_GeneralService/IGeneralService/GetFileSizeRespo" +
            "nse")]
        System.Threading.Tasks.Task<long> GetFileSizeAsync(string filePath);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IGeneralServiceChannel : POC.Business.GeneralService.IGeneralService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class GeneralServiceClient : System.ServiceModel.ClientBase<POC.Business.GeneralService.IGeneralService>, POC.Business.GeneralService.IGeneralService {
        
        public GeneralServiceClient() {
        }
        
        public GeneralServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public GeneralServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GeneralServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GeneralServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public POC.Business.GeneralService.ArchiveFileResult StartArchiveFile(string fileId, string sourcePath, string destinationPath) {
            return base.Channel.StartArchiveFile(fileId, sourcePath, destinationPath);
        }
        
        public System.Threading.Tasks.Task<POC.Business.GeneralService.ArchiveFileResult> StartArchiveFileAsync(string fileId, string sourcePath, string destinationPath) {
            return base.Channel.StartArchiveFileAsync(fileId, sourcePath, destinationPath);
        }
        
        public POC.Business.GeneralService.ArchiveFileStatus GetArchiveStatus(string fileId) {
            return base.Channel.GetArchiveStatus(fileId);
        }
        
        public System.Threading.Tasks.Task<POC.Business.GeneralService.ArchiveFileStatus> GetArchiveStatusAsync(string fileId) {
            return base.Channel.GetArchiveStatusAsync(fileId);
        }
        
        public POC.Business.GeneralService.CopyFileResult StartCopyFile(string fileId, string sourcePath, string uploadPath) {
            return base.Channel.StartCopyFile(fileId, sourcePath, uploadPath);
        }
        
        public System.Threading.Tasks.Task<POC.Business.GeneralService.CopyFileResult> StartCopyFileAsync(string fileId, string sourcePath, string uploadPath) {
            return base.Channel.StartCopyFileAsync(fileId, sourcePath, uploadPath);
        }
        
        public POC.Business.GeneralService.CopyFileStatus GetCopyStatus(string fileId) {
            return base.Channel.GetCopyStatus(fileId);
        }
        
        public System.Threading.Tasks.Task<POC.Business.GeneralService.CopyFileStatus> GetCopyStatusAsync(string fileId) {
            return base.Channel.GetCopyStatusAsync(fileId);
        }
        
        public POC.Business.GeneralService.PhysicalFolder[] GetAllFolders() {
            return base.Channel.GetAllFolders();
        }
        
        public System.Threading.Tasks.Task<POC.Business.GeneralService.PhysicalFolder[]> GetAllFoldersAsync() {
            return base.Channel.GetAllFoldersAsync();
        }
        
        public POC.Business.GeneralService.PhysicalFile[] GetFiles(string path) {
            return base.Channel.GetFiles(path);
        }
        
        public System.Threading.Tasks.Task<POC.Business.GeneralService.PhysicalFile[]> GetFilesAsync(string path) {
            return base.Channel.GetFilesAsync(path);
        }
        
        public POC.Business.GeneralService.PhysicalFolder[] GetChilds(string path) {
            return base.Channel.GetChilds(path);
        }
        
        public System.Threading.Tasks.Task<POC.Business.GeneralService.PhysicalFolder[]> GetChildsAsync(string path) {
            return base.Channel.GetChildsAsync(path);
        }
        
        public int CreateSourceFolder(string folderPath) {
            return base.Channel.CreateSourceFolder(folderPath);
        }
        
        public System.Threading.Tasks.Task<int> CreateSourceFolderAsync(string folderPath) {
            return base.Channel.CreateSourceFolderAsync(folderPath);
        }
        
        public long GetFileSize(string filePath) {
            return base.Channel.GetFileSize(filePath);
        }
        
        public System.Threading.Tasks.Task<long> GetFileSizeAsync(string filePath) {
            return base.Channel.GetFileSizeAsync(filePath);
        }
    }
}
