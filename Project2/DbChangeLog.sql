﻿drop table bsd_Log
drop table poc_ArchiveType_Field  
drop table poc_Document
drop table poc_Record
drop table poc_Field 
drop table poc_ArchiveType  

create table bsd_Log(
	Id [bigint] IDENTITY(1,1) NOT NULL primary key,
	[ApplicationId] [uniqueidentifier] NULL,
	[UserId] [uniqueidentifier] NULL,
	[Date] [datetime] NOT NULL,
	[Level] [varchar](32) NOT NULL,
	[Logger] [varchar](32) NOT NULL,
	[Exception] [varchar](max) NULL,
	[Message] [nvarchar](max) NULL, 
)  
GO
--Danh sách field--------------------
create table poc_Field(
FieldId uniqueidentifier not null primary key,
[Name] nvarchar(256) not null,
[Code] nvarchar(256) not null,
[Description] nvarchar(max) null,

[FormlyContent] nvarchar(max) not null,--cấu trúc 1 control

--Fix field dùng chung 
[IsRecordCommonField] bit not null default 0,
[IsDocumentCommonField] bit not null default 0,
 

[CreatedOnDate] [datetime] NOT NULL,
[LastModifiedOnDate] [datetime] NOT NULL,
[CreatedByUserId] [uniqueidentifier] NOT NULL,
[LastModifiedByUserId] [uniqueidentifier] NOT NULL,
)
GO
--Danh sách loại hình tài liệu--------------------
create table poc_ArchiveType(
ArchiveTypeId uniqueidentifier not null primary key,
[Name] nvarchar(256) not null,
[Code] nvarchar(256) not null,
[Description] nvarchar(max) null,

[Status] int  not null default 0,
[Order] int  not null default 0,


[CreatedOnDate] [datetime] NOT NULL,
[LastModifiedOnDate] [datetime] NOT NULL,
[CreatedByUserId] [uniqueidentifier] NOT NULL,
[LastModifiedByUserId] [uniqueidentifier] NOT NULL,

)  
GO
--Map field Nào thuộc hồ sơ nào |
create table poc_ArchiveType_Field(
ArchiveTypeFieldId bigint not null identity(1,1) primary key ,
FieldId uniqueidentifier not null CONSTRAINT FK_ArchiveType_Field_Field
                                        foreign key references poc_Field(FieldId),
ArchiveTypeId uniqueidentifier not null CONSTRAINT FK_ArchiveType_Field_ArchiveType
                                        foreign key references poc_ArchiveType(ArchiveTypeId),
[Order] int not null default 0,
[Type] int not null default 0,--0:Record|1:Document
[DisplayCategory]  bit not null default 0,
[DisplaySearchCategory]  bit not null default 0,
[DisplayReport]  bit not null default 0,
[DisplaySearchReport]  bit not null default 0, 
)
GO

--Danh sách hồ sơ--------------------
create table poc_Record(
RecordId uniqueidentifier not null primary key,
[Name] nvarchar(256) not null,
[Code] nvarchar(256) not null,
[Description] nvarchar(max) null,

ArchiveTypeId uniqueidentifier not null CONSTRAINT FK_Record_ArchiveType
                                        foreign key references poc_ArchiveType(ArchiveTypeId),--thuộc loại hình tài liệu nào

[CreatedOnDate] [datetime] NOT NULL,
[LastModifiedOnDate] [datetime] NOT NULL,
[CreatedByUserId] [uniqueidentifier] NOT NULL,
[LastModifiedByUserId] [uniqueidentifier] NOT NULL,

)  
GO
--Danh sách văn bản-------------------- 
create table poc_Document 
(
DocumentId uniqueidentifier not null primary key,
[Name] nvarchar(256) not null,
[Code] nvarchar(256) not null,
[Description] nvarchar(max) null,

ArchiveTypeId uniqueidentifier not null CONSTRAINT FK_Document_ArchiveType
                                        foreign key references poc_ArchiveType(ArchiveTypeId),--thuộc loại hình tài liệu nào

[CreatedOnDate] [datetime] NOT NULL,
[LastModifiedOnDate] [datetime] NOT NULL,
[CreatedByUserId] [uniqueidentifier] NOT NULL,
[LastModifiedByUserId] [uniqueidentifier] NOT NULL,
)  
GO